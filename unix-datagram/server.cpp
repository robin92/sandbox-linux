#include <iostream>
#include <thread>

#include <cstring>

#include <sys/fcntl.h>
#include <sys/signal.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/unistd.h>

#include <boost/scope_exit.hpp>

namespace {

template <class Callable> decltype(auto) silent_except(Callable callable) noexcept
{
    try
    {
        return callable();
    }
    catch (const std::exception& exc)
    {
        std::cerr << exc.what() << "\n";
        return -1;
    }
    catch (...)
    {
        std::cerr << "unexpected exception\n";
        return -1;
    }
}

namespace req {

bool validate(int err) { return err >= 0; }

} // namespace req

#define require(expr) {\
    if (const auto err = expr; not req::validate(err)) {\
        std::cerr << "ERR " #expr ": " << strerror(err) << " (" << std::to_string(err) << ")\n";\
        throw std::runtime_error("fatal error");\
    }\
}

std::pair<int, sockaddr_un> make_unix_dgram_sock(const char* path) noexcept
{
    constexpr auto family = AF_UNIX;
    auto out = std::make_pair(socket(family, SOCK_DGRAM, 0), sockaddr_un());
    memset(&out.second, 0, sizeof(sockaddr_un));
    out.second.sun_family = family;
    strncpy(out.second.sun_path, path, strlen(path));
    return out;
}

bool active = true;

void signal_handler(int)
{
    active = false;
}

int run(int, char** argv)
{
    auto [sockFd, addr] = make_unix_dgram_sock(argv[1]);
    require(sockFd);

    fcntl(sockFd, F_SETFL, O_NONBLOCK);
    BOOST_SCOPE_EXIT(sockFd) {
        close(sockFd);
    }
    BOOST_SCOPE_EXIT_END;

    require(bind(sockFd, reinterpret_cast<sockaddr*>(&addr), sizeof(decltype(addr))));
    BOOST_SCOPE_EXIT(argv) {
        unlink(argv[1]);
    }
    BOOST_SCOPE_EXIT_END;

    struct sigaction action, defAction;
    memset(&action, 0, sizeof(struct sigaction));
    memset(&defAction, 0, sizeof(struct sigaction));
    action.sa_handler = signal_handler;

    sigaction(SIGINT, &action, &defAction);
    BOOST_SCOPE_EXIT(&defAction) {
        ::sigaction(SIGINT, &defAction, nullptr);
    }
    BOOST_SCOPE_EXIT_END;

    while (active)
    {
        constexpr auto maxSize = 256u;
        std::array<uint8_t, maxSize> message = {0};
        sockaddr_un senderAddr;
        uint32_t senderSize = sizeof(senderAddr);
        const int bytesRead = recvfrom(
            sockFd, message.data(), message.size(), 0, reinterpret_cast<sockaddr*>(&senderAddr), &senderSize);
        if (bytesRead <= 0)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(33));
            continue;
        }
        std::cout << "sender: " << senderAddr.sun_path << " " << "message: " << message.data();
    }
    return 0;
}

}  // namespace

int main(int argc, char** argv)
{
    return silent_except([=] { return run(argc, argv); });
}
