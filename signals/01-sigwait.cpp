#include <cstring>
#include <iostream>
#include <signal.h>

static int required_return;

#define require(cmd) {\
    required_return = cmd;\
    if (required_return != 0) {\
        std::cerr << #cmd << "@:" << __LINE__ << " failed: " << std::strerror(errno) << "\n";\
        exit(-1);\
    }\
}

#define require1(cmd) {\
    required_return = cmd;\
    if (required_return != 0) {\
        std::cerr << #cmd << "@:" << __LINE__ << " failed: " << std::strerror(required_return) << "\n";\
        exit(-1);\
    }\
}

int main(int, char**) {
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    sigaddset(&set, SIGINT);

    require(sigprocmask(SIG_BLOCK, &set, nullptr));

    // Signals from set are now blocked, that is their handlers will NOT be
    // invoked. However, the signal will be marked as PENDING. Such signals
    // may be read with sigwait.

    for (bool run = true; run;) {
        int signal = 0;

        // Function sigwait will block until one of signals from set has been
        // marked as PENDING.
        require1(sigwait(&set, &signal));
        switch (signal) {
            case SIGINT:
                run = false;
            default:
                std::cout << "signal caught: " << signal << "\n";
                break;
        }
    }

    return 0;
}
