#include <atomic>
#include <chrono>
#include <condition_variable>
#include <ctime>
#include <iostream>
#include <mutex>
#include <string_view>
#include <thread>

#include <cassert>
#include <cstdlib>
#include <cstring>

#include <unistd.h>
#include <poll.h>
#include <sys/mman.h>
#include <sys/timerfd.h>

thread_local int require_return = 0;

#define require(cmd) {\
    require_return = cmd;\
    if (require_return < 0) {\
        std::cerr << "command '" << #cmd << "' failed: " << std::strerror(errno) << "\n";\
        exit(-1);\
    }\
}

namespace {

using namespace std::chrono_literals;

void closefd(int& fd) {
    if (fd >= 0) {
        close(fd);
        fd = -1;
    }
}

template <size_t N> void closefd(int (&fds)[N]) {
    for (size_t i = 0; i < N; i++) {
        closefd(fds[i]);
    }
}

void join(std::thread& thr) {
    if (thr.joinable()) {
        thr.join();
    }
}

class Tmpfile {
    char _filename[15] = "mmap-01-XXXXXX";
    int _fd = -1;

public:
    Tmpfile();
    ~Tmpfile();

    explicit operator const int&() const;

    explicit operator const char*() const;

    int fd() const;
    std::string_view name() const;
};

Tmpfile::Tmpfile() {
    require(mkstemp(_filename));
    _fd = require_return;
    if (_fd < 0) {
        throw std::runtime_error{"invalid fd: " + std::to_string(_fd)};
    }
}

Tmpfile::~Tmpfile() {
    closefd(_fd);
}

Tmpfile::operator const int&() const { return _fd; }

Tmpfile::operator const char*() const { return _filename; }

int Tmpfile::fd() const { return static_cast<const int&>(*this); }

std::string_view Tmpfile::name() const { return static_cast<const char*>(*this); }

void updating(std::condition_variable& cv, std::atomic_bool& run, int fd) {
    assert(fd >= 0);
    using Clock = std::chrono::high_resolution_clock;
    
    constexpr auto sleepInterval = 10s;
    int ctr = 0;
    std::mutex mtx;
    while (run.load()) {
        std::unique_lock lk{mtx};
        lseek(fd, 0, SEEK_SET);
        require(write(fd, &ctr, sizeof(ctr)));
        const auto last = Clock::now();
        cv.wait_for(lk, sleepInterval, [&] { return (Clock::now() - last >= sleepInterval) or not run; });
        ctr++;
    }
}

} // namespace

int main(int, char**) try {
    Tmpfile file;
    std::cout << "operating on file: " << file.name() << "\n";

    std::condition_variable cv;
    std::atomic_bool run {true};
    std::thread updater{updating, std::ref(cv), std::ref(run), file.fd()};

    void* addr = mmap(nullptr, sizeof(int), PROT_READ, MAP_PRIVATE, file.fd(), 0);
    if (addr == MAP_FAILED) {
        std::cerr << "mmap failed: " << std::strerror(errno) << "\n";
        exit(-1);
    }

    std::array<pollfd, 2> events;
    memset(events.data(), 0, events.size());
    events[0].fd = STDIN_FILENO;
    events[0].events = POLLIN;

    constexpr auto printIntervalSec = (1s).count();
    require(timerfd_create(CLOCK_MONOTONIC, 0));
    events[1].fd = require_return;
    events[1].events = POLLIN;

    itimerspec timerSpec;
    memset(&timerSpec, 0, sizeof(timerSpec));
    timerSpec.it_interval.tv_sec = printIntervalSec;
    timerSpec.it_value.tv_sec = printIntervalSec;
    timerfd_settime(events[1].fd, 0, &timerSpec, nullptr);

    while (run.load()) {
        poll(events.data(), events.size(), (10ms).count());

        for (auto&& ev : events) {
            if (ev.revents == 0) {
                continue;
            }

            if (ev.fd == STDIN_FILENO) {
                run = false;
            }
            else {
                uint64_t timerExpirationCtr = 0;
                read(ev.fd, &timerExpirationCtr, sizeof(uint64_t));
                auto* ptr = static_cast<int*>(addr);
                std::cout << *ptr << "\n";
            }
        }
    }

    cv.notify_one();

    closefd(events[1].fd);
    munmap(addr, sizeof(int));
    join(updater);
    return 0;
}
catch (...) {
    throw;
}
